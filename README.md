# About-Me-Chatbot
A chatbot that can answer user's questions about me. The chatbot runs on a client-side environment. The chatbot was trained locally and user's messages will not be stored in the database.

To develop the webpage I used Angular and Nebular. To develop and train the chatbot I used TensorFlowJS and Aida. I used a web worker for heavy cpu tasks like the chatbot's recognition.   

I'm planning to use the chatbot on my portfolio website. You can talk to the chatbot on https://igorarshinov.gitlab.io/about-me-chatbot.
