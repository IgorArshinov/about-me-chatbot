export enum NebularThemesEnum {
  Default = "default",
  Dark = "dark",
  Cosmic = "cosmic",
}
