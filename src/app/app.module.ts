import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { ChatbotComponent } from './components/chatbot/chatbot.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { WorkerModule } from "angular-web-worker/angular";
import { ChatbotWorker } from "./components/chatbot/chatbot.worker";
import {
  NbAccordionModule,
  NbButtonModule,
  NbCardModule,
  NbChatModule, NbContextMenuModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule, NbMenuModule, NbPopoverModule, NbRadioModule,
  NbSpinnerModule,
  NbThemeModule
} from "@nebular/theme";
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { FormsModule } from "@angular/forms";
import { NebularThemesEnum } from "./enums/nebular-themes.enum";


@NgModule({
  declarations: [
    AppComponent,
    ChatbotComponent,
  ],
  imports: [
    BrowserModule,
    NbMenuModule.forRoot(),
    BrowserAnimationsModule,
    NbThemeModule.forRoot({name: NebularThemesEnum.Default}),
    NbLayoutModule,
    NbEvaIconsModule,
    AppRoutingModule,
    NbChatModule,
    MatProgressSpinnerModule,
    WorkerModule.forWorkers([
      {worker: ChatbotWorker, initFn: () => new Worker('./components/chatbot/chatbot.worker.ts', {type: 'module'})},
    ]),
    FormsModule,
    NbInputModule,
    NbIconModule,
    NbSpinnerModule,
    NbCardModule,
    NbButtonModule,
    NbPopoverModule,
    NbContextMenuModule,
    NbAccordionModule,
    NbRadioModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
