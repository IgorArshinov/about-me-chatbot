import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatbotComponent } from "./components/chatbot/chatbot.component";
import { AppComponent } from "./app.component";

const routes: Routes = [
  {
    path: '',
    component: ChatbotComponent,
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
