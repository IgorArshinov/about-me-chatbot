import * as tf from '@tensorflow/tfjs';
import EnglishTokenizer from '../languages/en/EnglishTokenizer';
import NerModel from '../models/NerModel';
import { EmbeddingsModel } from '../models/EmbeddingsModel';
import {
  IAidaTokenizer,
  IDatasetParams,
  IPipelineDefinition,
  IPipelineModelLogger,
  ISlotReducer,
  ITestingParams,
  ITrainingParams,
  ITrainStatsHandler, PretrainedDict
} from '../interfaces/types';
import ClassificationModel from '../models/ClassificationModel';


function getTokenizer(language: 'en' | 'es') {
  const lang = language ? language.toLowerCase() : language;
  if (lang === 'en') {
    return EnglishTokenizer;
  } else if (lang === 'es') {
    // return SpanishTokenizer;
  }
  throw new Error('Invalid config language. Only \'en\' and \'es\' are supported.');
}

export const defaultPipelineDefinition: IPipelineDefinition = {
  config: {
    classification: {
      epochs: 5,
      filterSizes: [2, 4, 8],
      lowConfidenceThreshold: 0.3,
      numFilters: 128
    },
    default: {
      batchSize: 120,
      drop: 0.5,
      embeddingDimensions: 300,
      lossThresholdToStopTraining: 1e-6,
      maxNgrams: 20,
      trainingValidationSplit: 0.3
    },
    ner: {
      addAttention: true,
      epochs: 5,
      lowConfidenceThreshold: 0.2,
      numFilters: [128, 128],
      rnnUnits: 100
    }
  }
};

export class AidaPipeline {
  private _pipelineDefinition: IPipelineDefinition = defaultPipelineDefinition;
  private _datasetParams: IDatasetParams;
  private _embeddingsModel: EmbeddingsModel;
  private _classificationModel: ClassificationModel;
  private _nerModel: NerModel;
  private _logger: IPipelineModelLogger;
  private _tokenizer: IAidaTokenizer;
  private _ngramToIdDictionary: { [p: string]: number };

  constructor(cfg: {
    tokenizer?: IAidaTokenizer;
    nerModel?: NerModel;
    classificationModel?: ClassificationModel;
    embeddingsModel?: EmbeddingsModel;
    datasetParams?: IDatasetParams;
    logger?: IPipelineModelLogger;
    ngramToIdDictionary?: { [key: string]: number };
    trainStatsHandler?: ITrainStatsHandler;
    pipelineDefinition?: IPipelineDefinition;
    pretrainedClassifier?: tf.LayersModel;
    pretrainedNer?: tf.LayersModel;
    pretrainedEmbedding?: tf.LayersModel;
    pretrainedNGramVectors?: PretrainedDict;
  }) {
    if (cfg.pipelineDefinition) {
      this._pipelineDefinition = cfg.pipelineDefinition;
    }
    this._datasetParams = cfg.datasetParams;
    this._logger = cfg.logger;
    this._tokenizer = getTokenizer(this._datasetParams.language);
    if (cfg.embeddingsModel) {
      this._embeddingsModel = cfg.embeddingsModel;
    } else {
      this._embeddingsModel = new EmbeddingsModel(
        this._ngramToIdDictionary = cfg.ngramToIdDictionary,
        cfg.datasetParams.maxWordsPerSentence,
        this._pipelineDefinition.config.default.maxNgrams,
        this._pipelineDefinition.config.default.embeddingDimensions,
        this._tokenizer,
        cfg.pretrainedEmbedding,
        cfg.pretrainedNGramVectors
      );
    }
    if (cfg.classificationModel) {
      this._classificationModel = cfg.classificationModel;
    } else {
      const classificationCfg = Object.assign({}, this._pipelineDefinition.config.default, this._pipelineDefinition.config.classification);
      let classificationTrainStatsHandler;

      if (cfg.trainStatsHandler) {
        classificationTrainStatsHandler = cfg.trainStatsHandler.classification;
      }
      this._classificationModel = new ClassificationModel(
        classificationCfg,
        this._datasetParams,
        this._embeddingsModel,
        this._logger,
        cfg.pretrainedClassifier,
        classificationTrainStatsHandler
      );
    }

    if (cfg.nerModel) {
      this._nerModel = cfg.nerModel;
    } else {
      let nerTrainStatsHandler;
      if (cfg.trainStatsHandler) {
        nerTrainStatsHandler = cfg.trainStatsHandler.ner;
      }
      const nerCfg = Object.assign({}, this._pipelineDefinition.config.default, this._pipelineDefinition.config.ner);
      this._nerModel = new NerModel(
        nerCfg,
        this._datasetParams,
        this._embeddingsModel,
        this._logger,
        cfg.pretrainedNer,
        nerTrainStatsHandler
      );
    }
  }


  public train = async (trainDataset: ITrainingParams): Promise<void> => {
    this._logger.log('START TRAINING PIPELINE MODELS!');
    this._logger.log('==================================================================================================');
    await this._classificationModel.train(trainDataset);
    const slotsLength = Object.keys(this._datasetParams.slotsToId).length;
    // NOTE: only train the ner model if there are slots in the training params
    if (slotsLength >= 2) {
      await this._nerModel.train(trainDataset);
    } else {
      this._logger.log('SKIPPING NER MODEL (NO SLOTS FOUND)!');
      this._logger.log('==================================================================================================');
    }
    this._logger.log('FINISHED TRAINING PIPELINE MODELS!');
    this._logger.log('==================================================================================================');
  };

  public test = async (testDataset: ITestingParams) => {
    this._logger.log('START TESTING PIPELINE MODELS!');
    this._logger.log('==================================================================================================');
    const classificationStats = await this._classificationModel.test(testDataset);
    // NOTE: only use the ner model if there are slots in the training params
    const slotsLength = Object.keys(this._datasetParams.slotsToId).length;
    const nerStats = slotsLength >= 2 ? await this._nerModel.test(testDataset) : {correct: 0, wrong: 0};
    return {classificationStats, nerStats};
  };

  public recognize = (sentences: string[]) => {
    const classification = this._classificationModel.predict(sentences);
    // NOTE: only use the ner model if there are slots in the training params
    const slotsLength = Object.keys(this._datasetParams.slotsToId).length;
    const ner: ISlotReducer[] = slotsLength >= 2 ? this._nerModel.predict(sentences, classification) : [];
    return {classification, ner};
  };

  public save = async (cfg: { classificationPath: string; nerPath: string; embeddingPath: string }) => {
    this._logger.log('SAVING PIPELINE MODELS!');
    this._logger.log('==================================================================================================');
    await this._classificationModel.tfModel().save(cfg.classificationPath);
    const slotsLength = Object.keys(this._datasetParams.slotsToId).length;
    // NOTE: only download the ner model if there are slots
    if (slotsLength >= 2) {
      await this._nerModel.tfModel().save(cfg.nerPath);
    }
    await this._embeddingsModel.tfModel().save(cfg.embeddingPath);
  };
}
