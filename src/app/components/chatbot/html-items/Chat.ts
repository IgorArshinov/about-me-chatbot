import { Message } from "./Message";

export class Chat {

  private _status: string;
  private readonly _title: string;
  private _messages: Message[] = [];

  constructor(status: string, title: string) {
    this._status = status;
    this._title = title;
  }

  set status(value: string) {
    this._status = value;
  }

  get messages(): Message[] {
    return this._messages;
  }

  get title(): string {
    return this._title;
  }

  get status(): string {
    return this._status;
  }
}
