import { User } from "./User";

export class Message {
  private _date: Date;
  private _text: string;
  private _reply: boolean;
  private _user: User;
  private _type: any;
  private _files: any;

  get date(): Date {
    return this._date;
  }

  get text(): string {
    return this._text;
  }

  get reply(): boolean {
    return this._reply;
  }

  get user(): User {
    return this._user;
  }

  get type(): any {
    return this._type;
  }

  get files(): any {
    return this._files;
  }

  constructor(text: string, date: Date, reply: boolean, user: User) {
    this._text = text;
    this._date = date;
    this._reply = reply;
    this._user = user;
  }
}
