export class User {
  private readonly _name: string;
  private readonly _avatar: string;

  get name(): string {
    return this._name;
  }

  get avatar(): string {
    return this._avatar;
  }

  constructor(name: string, avatar: string) {
    this._avatar = avatar;
    this._name = name;
  }
}
