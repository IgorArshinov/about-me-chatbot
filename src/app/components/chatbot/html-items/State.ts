export class State {
  private _modelsNotLoaded: boolean;
  private _showChatbotRecognitionTemporaryText: boolean;
  private _chatbotRecognitionCardAccent: string;

  constructor(modelsNotLoaded: boolean, showChatbotPredictionTemporaryText: boolean, chatbotPredictionCardAccent: string) {
    this._modelsNotLoaded = modelsNotLoaded;
    this._showChatbotRecognitionTemporaryText = showChatbotPredictionTemporaryText;
    this._chatbotRecognitionCardAccent = chatbotPredictionCardAccent;
  }

  get modelsNotLoaded(): boolean {
    return this._modelsNotLoaded;
  }

  set modelsNotLoaded(value: boolean) {
    this._modelsNotLoaded = value;
  }

  get showChatbotRecognitionTemporaryText(): boolean {
    return this._showChatbotRecognitionTemporaryText;
  }

  set showChatbotRecognitionTemporaryText(value: boolean) {
    this._showChatbotRecognitionTemporaryText = value;
  }

  get chatbotRecognitionCardAccent(): string {
    return this._chatbotRecognitionCardAccent;
  }

  set chatbotRecognitionCardAccent(value: string) {
    this._chatbotRecognitionCardAccent = value;
  }
}
