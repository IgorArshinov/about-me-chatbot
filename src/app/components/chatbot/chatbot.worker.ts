import { AngularWebWorker, bootstrapWorker, Callable, OnWorkerInit, Subscribable } from 'angular-web-worker';
import { AidaPipeline } from "../../pipelines/AidaPipeline";
import * as tf from "@tensorflow/tfjs";
import axios from "axios";
import { BehaviorSubject } from "rxjs";
import { Logger } from "../../singletons/Logger";
import BirthdayCalculator from "../../utilities/BirthdayCalculator";

/// <reference lib="webworker" />

@AngularWebWorker()
export class ChatbotWorker implements OnWorkerInit {

  private pipeline: AidaPipeline;

  private currentAge: number = BirthdayCalculator.getCurrentAge("1991-01-15");
  private daysOld: number = BirthdayCalculator.getHowManyDaysOld("1991-01-15");

  @Subscribable() modelsNotLoaded: BehaviorSubject<boolean>;

  async onWorkerInit() {
    await this.loadSavedModels().then(() => {
      this.modelsNotLoaded = new BehaviorSubject<boolean>(true);
    });
  }

  @Callable()
  async recognize(input: string): Promise<{ recognition: any; response: any }> {

    const recognition = await this.pipeline.recognize([input]);

    const classification = recognition.classification[0];
    let response;
    if (!classification || classification.confidence < 0.35) {
      response = ['I\'m not sure what you mean, because I haven\'t learned all the words yet.'];
    } else {
      const responseMap: { [key: string]: () => Promise<any> } = {
        greet: async () => ['Hey! how can I help you?'],
        bye: async () => ['Bye, have fun!'],
        affirmative: async () => ['Great!'],
        negative: async () => ['Ok, I understand no. Maybe later you will change your mind'],
        unknown: async () => ['Hm... I\'m just an AI assistant.', 'How can I help you?'],
        isBeingRudeToChatbot: async () => ['I dont have emotions, but that doesn\'t mean that you can be rude to me!'],
        somethingAboutIgor: async () => ['What do you wish to know about Igor?'],
        question: async () => ['Can you please ask a more specific question?'],
        wantsToKnowIgorHobbies: async () => ['In his free time Igor loves to create art, play sports, program, read scientific books and travel to new places.'],
        wantsToKnowWhatIgorStudied: async () => ['In 2010 Igor studied Graphic Design at PXL and he got his Master of Arts in Visual Arts in 2014. He specialized in Game & Digital Design.', 'From 2014 to February 2015 he followed the Specific Teacher Training in Audiovisual and Visual Arts at PXL.', 'In 2016 he started with the Switch2IT course at PXL and he wants to earn a Bachelor of Applied Information Technology.'],
        wantsToKnowWhoIsIgor: async () => ['Igor was born in Kazakhstan. In 2000 he moved to Belgium when he was 9 years old.', 'He is now ' + this.currentAge + ' years and ' + this.daysOld + ' days old.'],
        wish: async () => ['What do you wish? Can you please be more specific?'],
      };
      response = responseMap[classification.intent]
        ? await responseMap[classification.intent]()
        : ['I\'m not sure what you mean, because I haven\'t learned all the words yet.', 'How can I help you?'];
    }
    return {response, recognition: recognition};
  }

  async downloadsTrainedModel(backend: 'web' | 'node' | 'keras') {
    const modelsUrls = {
      // keras: {
      //   classification: '/models/pretrained/keras/classification/model.json',
      //   embedding: '/models/pretrained/keras/embedding/model.json',
      //   ner: '/models/pretrained/keras/ner/model.json'
      // },
      node: {
        classification: 'assets/models/node/classification/model.json',
        embedding: 'assets/models/node/embedding/model.json',
        ner: 'assets/models/node/ner/model.json'
      },
      // web: {
      //   classification: '/models/pretrained/web/classification/classification.json',
      //   embedding: '/models/pretrained/web/embedding/embedding.json',
      //   ner: '/models/pretrained/web/ner/ner.json'
      // }
    };
    const pretrainedEmbedding = await tf.loadLayersModel(modelsUrls[backend].embedding, {strict: false});
    const pretrainedClassifier = await tf.loadLayersModel(modelsUrls[backend].classification);
    const pretrainedNer = await tf.loadLayersModel(modelsUrls[backend].ner);
    return {pretrainedEmbedding, pretrainedClassifier, pretrainedNer};
  };


  async loadSavedModels() {
    const files = ['assets/models/ngram_to_id_dictionary.json', 'assets/models/dataset_params.json'];
    const jsonFiles = await this.downloadFiles(files);
    const ngramToIdDictionary = jsonFiles[0].data;
    const datasetParams = jsonFiles[1].data;
    const {pretrainedClassifier, pretrainedNer, pretrainedEmbedding} = await this.downloadsTrainedModel('node');
    const pipeline = new AidaPipeline({
      logger: Logger.getInstance(),
      datasetParams,
      ngramToIdDictionary,
      pretrainedClassifier,
      pretrainedEmbedding,
      pretrainedNer
    });
    this.pipeline = pipeline;
    return pipeline;
  };

  async downloadFiles(files: string[]) {
    let total = 0;
    let progress = 0;
    return await Promise.all(
      files.map(file =>
        axios.get(file, {
          onDownloadProgress: progressEvent => {
            const totalLength = Math.round((progressEvent.loaded / progressEvent.total) * 100);
            // const totalLength = progressEvent.lengthComputable
            //   ? progressEvent.total
            //   : progressEvent.target.getResponseHeader('content-length') ||
            //   progressEvent.target.getResponseHeader('x-decompressed-content-length');
            // if (totalLength !== null) {
            //   total += totalLength;
            //   progress += Math.round((progressEvent.loaded * 100) / total);
            // }
          }
        })
      )
    );
  };
}
bootstrapWorker(ChatbotWorker);
