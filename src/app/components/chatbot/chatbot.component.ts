import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import * as tf from '@tensorflow/tfjs';
import { WorkerManager, WorkerClient } from 'angular-web-worker/angular';
import { ChatbotWorker } from "./chatbot.worker";
import { Subscription } from 'rxjs';
import { Chat } from "./html-items/Chat";
import { Message } from "./html-items/Message";
import { User } from "./html-items/User";
import { State } from "./html-items/State";
import { NbThemeService } from "@nebular/theme";
import { NebularThemesEnum } from 'src/app/enums/nebular-themes.enum';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.scss']
})
export class ChatbotComponent implements OnInit, OnDestroy, AfterViewInit {

  private client: WorkerClient<ChatbotWorker>;
  public botRecognition;
  public state = new State(true, true, "warning");
  private subscription: Subscription;
  public chat: Chat = new Chat('warning', 'Chat Window');
  public menuItems: any = [{title: 'Profile'}, {title: 'Log out'}];
  public selectedTheme: string;

  constructor(private workerManager: WorkerManager, private themeService: NbThemeService) {
  }

  async ngOnInit(): Promise<void> {
    if (this.workerManager.isBrowserCompatible) {
      this.client = this.workerManager.createClient(ChatbotWorker);

    } else {
      this.client = this.workerManager.createClient(ChatbotWorker, true);

    }
    await this.client.connect();
    this.subscription = await this.client.subscribe(w => w.modelsNotLoaded,
      (isLoaded) => {
        if (isLoaded) {
          this.state.modelsNotLoaded = false;
          this.chat.status = "success";
          this.botSendsMessage('Hi!');
          this.state.chatbotRecognitionCardAccent = "success";
          this.client.unsubscribe(this.subscription);
        }
      }
    );
    this.selectedTheme = this.themeService.currentTheme;
  }

  ngAfterViewInit(): void {

  }

  botSendsMessage(message) {
    this.chat.messages.push(new Message(message, new Date(), false, new User('Bot', 'https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/robot-face.png')));
  }

  botReactionToTheUserMessage = async (message) => {

    const input = message.replace(/ +(?= )/g, '');

    const result = await this.client.call(w => w.recognize(input));
    this.botRecognition = result.recognition;

    if (this.state.showChatbotRecognitionTemporaryText) {
      this.state.showChatbotRecognitionTemporaryText = false
    }

    for (let i = 0, j = 1; i < result.response.length; i++, j++) {
      setTimeout(() => {
        this.botSendsMessage(result.response[i]);
      }, j * 500);
    }
  };

  async sendMessage(messages: any, event: any): Promise<void> {
    this.chat.messages.push(new Message(event.message, new Date(), true, new User('You', 'https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/robot-face.png')));
    await this.botReactionToTheUserMessage(event.message);
  }

  ngOnDestroy(): void {
    tf.disposeVariables();
    (window as any).tf = tf;
    this.client.destroy();
  }

  onThemeChange(theme: string) {
    switch (theme) {
      case NebularThemesEnum.Default: {
        this.themeService.changeTheme(NebularThemesEnum.Default);
        break;
      }
      case NebularThemesEnum.Dark: {
        this.themeService.changeTheme(NebularThemesEnum.Dark);
        break;
      }
      case NebularThemesEnum.Cosmic: {
        this.themeService.changeTheme(NebularThemesEnum.Cosmic);
        break;
      }
    }
  }
}


