import { IPipelineModelLogger } from "../interfaces/types";

export class Logger implements IPipelineModelLogger {

  private static instance: Logger;

  private constructor() {
  }

  public static getInstance(): Logger {
    if (!Logger.instance) {
      Logger.instance = new Logger();
    }

    return Logger.instance;
  }

  debug(t: any): void {
      return null;
  }

  error(t: any): void {
    console.error(t);
  }

  log(t: any): void {
    console.log(t);
  }

  warn(t: any): void {
    console.warn(t);
  }
}
