export default class BirthdayCalculator {
  private static _birthDate: Date;
  private static _currentDate: Date = new Date();

  private static set birthDate(date: Date) {
    if (!this._birthDate) {
      this._birthDate = date;
    }
  }

  static getCurrentAge(date: string) {
    this._birthDate = new Date(date);
    return this._currentDate.getFullYear() - this._birthDate.getFullYear();
  }

  static getHowManyDaysOld(date: string) {
    this._birthDate = new Date(date);
    let differenceInTime = new Date(this._birthDate.getTime());
    differenceInTime.setFullYear(this._currentDate.getFullYear())
    return Math.trunc((this._currentDate.getTime() - differenceInTime.getTime()) / (1000 * 3600 * 24));
  }
}
